int add(int a, int b)
{
	return a + b;
}

#include <string>

class Person {
private:
	int age;
	std::string name;

public:
	
	Person(std::string name, int age){
		this->age = age;
		this->name = name;
	}
	
	int getAge(){
		return age;
	}
	
	const char* getName(){
		return name.c_str();
	}
};
