#pragma once
#include <string>
int add(int a, int b);
class Person {
private:
	int age;
	std::string name;

public:
	
	Person(std::string name, int age);
	
	int getAge();
	
	const char* getName();
};
