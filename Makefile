OUT = main
CC = g++
ODIR = obj
SDIR = src
INC = -Iinc

CPP_FILES := $(wildcard $(SDIR)/*.cpp)

OBJ_FILES := $(notdir $(CPP_FILES:.cpp=.o))
OBJS = $(patsubst %,$(ODIR)/%,$(OBJ_FILES))

CFLAGS = -Wall -std=c++11

$(ODIR)/%.o: $(SDIR)/%.cpp 
	$(CC) -c $(INC) -o $@ $< $(CFLAGS)

$(OUT): $(OBJS) 
	$(CC) -o $(OUT) $^

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o $(OUT)
